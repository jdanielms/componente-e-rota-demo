import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit{

  constructor(
    private router: Router,
  ){}

  ngOnInit(): void {
    this.navchild1()
  }

  navchild1(){
    this.router.navigate(['master/child1'])
  }

  navchild2(){
    this.router.navigate(['master/child2'])
  }

}
