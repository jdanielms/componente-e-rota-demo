import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Child2Component } from './pages/child2.component';
import { Child2RoutingModule } from './child2.routing.module';



@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    Child2RoutingModule
  ]
})
export class Child2Module { }
