import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Btncomponent1Component } from './btncomponent1.component';

describe('Btncomponent1Component', () => {
  let component: Btncomponent1Component;
  let fixture: ComponentFixture<Btncomponent1Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Btncomponent1Component]
    });
    fixture = TestBed.createComponent(Btncomponent1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
