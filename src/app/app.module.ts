import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MasterComponent } from './modules/master/pages/master.component';
import { Btncomponent1Component } from './modules/btncomponent1/btncomponent1.component';
import { Child1Component } from './modules/child1/pages/child1.component';
import { Child2Component } from './modules/child2/pages/child2.component';



@NgModule({
  declarations: [
    Btncomponent1Component,
    AppComponent,
    MasterComponent,
    Child1Component,
    Child2Component
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
